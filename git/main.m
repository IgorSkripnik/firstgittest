//
//  main.m
//  git
//
//  Created by Igor Skripnik on 27.07.2018.
//  Copyright © 2018 garik. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
